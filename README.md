### READ ME ###

Repositório de aluguer de Trotinetes para a UC de Ambientes de Desenvolvimento Colaborativo do curso TeSP em Sistemas e Tecnologias de Informação da Universidade do Algarve

"A aplicação de aluguer os trotinetes em python"

### Como utiliizar? ###

git clone https://toyanju@bitbucket.org/giovana_cunha/ezyrider-adc.git

### Instalação de module Tabulate ###

pip install tabulate

### Funcionalidades ###

* Registar Nova trotinete
* Registar novo utilizador
* Registar novo aluguer
* Registar novo registo
- listar trotinetes
- listar utilizadores
- listar aluguer
- listar registo

### Menu Inicial ###

 ![menu](https://cdn.discordapp.com/attachments/908652607432708137/934385005063446558/unknown.png)
### Autores: ###

* Franclim João
* Giovana Cunha
* Toyanju Gurung