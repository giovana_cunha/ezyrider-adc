from io_terminal import imprime_lista

nome_ficheiro_lista_de_registo = "lista_de_registo.pk"


def cria_registo():

    numserie = input("numserie? ")
    data_hora = input("data_hora? ").upper()
    coordenadas_gps = input("coordenadas_gps? ")
    valor_carga = input("valor_carga? ")

    return {"numserie": numserie, "data_hora": data_hora,
            "coordenadas_gps": coordenadas_gps, "valor_carga": valor_carga}

def imprime_lista_de_registo(lista_de_registo):
    """ ..."""

    imprime_lista(cabecalho="Lista de registos", lista=lista_de_registo)
