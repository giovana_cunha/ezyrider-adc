.. ezy documentation master file, created by
   sphinx-quickstart on Mon Jan 17 14:00:15 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ezy's documentation!
===============================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   aluguer
   io_ficheiros
   io_terminal
   main
   registo
   trotinetas
   utilizadores


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
